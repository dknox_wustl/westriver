# West River build development notes
As of 1/4/2017

## General

The general strategy is to have a flat set of XML files and a flat collection of images, and to produce a flat set of HTML files that are essentially like materialized TEI Boilerplate without the browser doing the XSLT transform. We use XSLT not just to pre-make the HTML files, but also to aggregate collection-level information, including the table of contents and the Lunr full-text search index.

## Dependencies

### Build process:

| `bash` or equivalent
| `saxonhe` for XSLT 2.0
| `jing` for validation
| `jq` for minifying the Lunr JSON index

### Web:

| Pure CSS
| jQuery
| Lunr.js
| LZ-string to compress a copy of the Lunr index for browser local stroage


### Mapping:

| *ArcGIS Online*

## XSLT

`teibpmod.xsl` is a modified TEI boilerplate transformation. For our purposes it is not run directly, but is called from another transformation that processes all the files. The `htmlShell` named template establishes the page structure for all pages, including header and footer, search block, etc. Other key templates handle the treatment of images and the embedded maps. There is room to clean this up; some templates left over from TEI Boilerplate may be superfluous for our purposes.

`build_from_toc.xsl` is the primary engine. It uses `toc.xml` as a driver file. There is no need to give `saxonhe` an output file path, because the script uses result documents to create all HTML output, starting with an index file representing the table of contents, and followed by an HTML file for each inscription. Most specialized templates in this file apply to the index; otherwise the `htmlShell` template in `teibpmod.xsl` does the work.

## CSS

This uses the [Pure CSS](http://purecss.io) framework, which provides a relatively simple base for a responsive grid-based layout.

There is plenty of room for design improvements in local CSS.

## Lunr

The Lunr loading code copies a trick from the Bizet project and uses browser local storage, when available, to keep a local copy of the index for a fixed period of time rather than reload it on every page. It's kind of cool but could be cleaned up. For instance, rather than storing a timestamp, the build process could store version numbers in the JSON and in the JS, so that the index could be cached as long as necessary yet updated immediately whenever the JS notices that it's outdated.

## Mapping

The ArcGIS Online maps are a somewhat brittle external dependency. It is not clear how they will be updated over time. Tighter integration of information will need a different strategy. The raw location data is available for the maps we have in January 2017.

## Testing

There are currently a few scripts for sanity checking and validation.


