var StelaesMap = L.map('mastermap').setView([24.5, 109], 7);

L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoieXVtZW5nem91IiwiYSI6ImNqOGNqbjUyczBieGgzM281NnUwMDdlNWkifQ.w-dmxSvkb_bMEsl-rZNI5g', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.outdoor',
    accessToken: 'pk.eyJ1IjoieXVtZW5nem91IiwiYSI6ImNqOGNqbjUyczBieGgzM281NnUwMDdlNWkifQ.w-dmxSvkb_bMEsl-rZNI5g'
}).addTo(StelaesMap);


var fc = {
    "type": "FeatureCollection",
    "features":[]
};


/* Load JSON file and create a geoJSON object FeatureCollection */
(function makeGeoJSON() {
    var request = new XMLHttpRequest();
    request.open('GET', "/" + BASEPATH + "/js/StelaesMap.json", true);
    
    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            var resp = this.response;
            const data = JSON.parse(resp);
            for (i = 0; i < data.length; i++) {
//                var dispx = (0.5 - Math.random()) * 0.01;
//                var dispy = (0.5 - Math.random()) * 0.01;
                var feat = {
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates":[data[i].x, data[i].y]
                    },
                    "properties": data[i]
                }
                fc.features.push(feat);
            }
        } else {
            console.log("ERROR in getting /westriver/js/StelaesMap.json");
        }
    };
    
    request.send();
})();


/* use geoJSON layer in leaflet to add points and popups to the map */
function addPoints() {
    var newlayer = L.geoJSON(fc, {
        onEachFeature: function (feature, layer) {
            var links =[];
            for (i = 0; i < feature.properties.stelaes.length; i++) {
                links[i] = "<a href='stelae/" + feature.properties.stelaes[i] + ".html' style='font-size:12px'>" + feature.properties.stelaes[i] + "</a>";
            }
            
            var popup = "<span style='font-size:14px'>" + feature.properties.id + "</span>" + "<br/>" + links.join("<br/>");
            layer.bindPopup(popup);
            layer.on("mouseover", function (feature) {
                this.openPopup()
            });
        }
    }).addTo(StelaesMap);
}
