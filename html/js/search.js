var searchdata;
const BASEPATH = "westriver";

var setupsearch = function () {
    searchdata = {
    };
    
    
    function getJSON() {
        var request = new XMLHttpRequest();
        request.open('GET', '/' + BASEPATH + "/js/westriverindex.json", true);
        
        request.onload = function () {
            if (this.status >= 200 && this.status < 400) {
                var resp = this.response;
                const items = JSON.parse(resp);
                searchdata = {
                };
                searchdata.texts = {
                };
                for (i in items) {
                    searchdata.texts[items[i][ 'id']] = {
                        'title': items[i][ 'title'],
                        'body': items[i][ 'content']
                    };
                }
            } else {
                console.log("ERROR in getting /" + BASEPATH + "/js/westriverindex.json");
            }
        };
        
        request.send();
    }
    
    
    
    function activatesearch() {
        $("#searchbox").css("visibility", "visible");
        $("#resultlist").css("display", "none");
        $("#searchterm").on("input", function () {
            var term = $("#searchterm").val();
            var resultls =[];
            if (term !== '') {
                // input is a list of both regular and authentic search terms
                var input = altToNorm(simplifiedToTraditional(term));
                localStorage.setItem("searchterm", JSON.stringify(input));
                for (key in searchdata.texts) {
                    var doc = traditionalToSimplified(searchdata.texts[key][ 'title'] + searchdata.texts[key][ "body"]).toLowerCase();
                    for (var i = 0; i < input.length; i++) {
                        var wd = traditionalToSimplified(input[i]).toLowerCase();
                        if (doc.includes(wd)) {
                            resultls.push(key);
                            break;
                        }
                    }
                }
            }
            
            var addresult = function (i, section) {
                var htmlpath = resultls[i].replace(".xml", ".html");
                htmlpath = htmlpath.replace("src/", "/" + BASEPATH + "/stelae/");
                var li_str = "<li class=\"pure-u-1 pure-u-md-1-2 pure-u-lg-1-4\"><a href=\"" + htmlpath + "\">" +
                searchdata.texts[resultls[i]].title +"</a></li>";
                $(section).append(li_str);
            }
            
            $("#resultlist").html("");
            if (resultls.length) {
                if (resultls.length == 1) {
                    $("#hitcount").html("1 hit");
                } else {
                    $("#hitcount").html(resultls.length + " hits");
                }
                $("#resultlist").css("display", "flex");
            } else {
                $("#hitcount").html("");
                $("#resultlist").css("display", "none");
            }
            for (var i = 0; i < resultls.length; i++) {
                addresult(i, "#resultlist");
            }
            //            $("#resultlist").slideDown(1000);
        });
    };
    
    getJSON();
    activatesearch();
}