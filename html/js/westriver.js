/* highlight search terms */
function markSearchResults(){
    var termsArr = JSON.parse(localStorage.getItem("searchterm"));
    $("text").mark(termsArr);
    localStorage.clear();
    
    // jump to the section on the page where the search term first appears
    var queryArr = termsArr.map(function(x){return ":contains('" + x + "'):not(:has(div,list,item,ab))"});
    var queryStr = queryArr.join();
    var hitElementId = $(queryStr).attr("id");
    location.href = "#" + hitElementId;  
    window.scroll(window.scrollX, window.scrollY - 600);
}

window.onload=function(){
    const images = document.querySelectorAll("#imageset img");
    const displayimage = document.querySelector("#displayimage img");
    const thumb2full = function (thumbpath) {
        return thumbpath.replace("thumbnails/", "")
    };
    images.forEach(el =>
                   el.onclick = (
                       e => displayimage.src = thumb2full(el.src))
                  )
   
    if (typeof addPoints === "function"){
        addPoints();
    }
    if (typeof $().mark === "function" && localStorage.getItem("searchterm") !== null){
        markSearchResults();
    }
    setupsearch();  // from search.js
}
