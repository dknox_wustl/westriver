<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:html="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xsl tei #default">

	<xsl:template match="/">
		<xsl:apply-templates select="files" mode="name"/>
	</xsl:template>	

	<xsl:template match="files" mode="name">
		<xsl:apply-templates select="document(file/@n)//tei:name" mode="name"/>
	</xsl:template>	
	
	<xsl:template match="tei:name" mode="name">
		<xsl:value-of select="."/>
		<xsl:text>&#x0a;</xsl:text>
	</xsl:template>

</xsl:stylesheet>
