<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:hdw="http://hdw.artsci.wustl.edu/ns"
    xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs hdw" version="2.0">


    <xsl:output omit-xml-declaration="yes"/>

    <xsl:template match="/">
        <xsl:text>&#x0a;Filename doesn't match id:&#x0a;</xsl:text>
        <xsl:apply-templates select="files/file" mode="filename_not_id"/>
        
        <xsl:text>&#x0a;Compare filename and title:&#x0a;</xsl:text>
        <xsl:apply-templates select="files/file" mode="filename_vs_title"/>
        
    </xsl:template>

    <xsl:template match="file" mode="filename_not_id">
        <xsl:variable name="doc" select="document(@n)"/>
        <xsl:variable name="filebase" select="replace(replace(@n, 'src/', ''), '.xml', '')"/>
        <xsl:variable name="idno" select="$doc//tei:idno"/>
        <xsl:if test="not($idno eq $filebase)">
            <xsl:text>&#x09;</xsl:text>
            <xsl:value-of select="$filebase"/>
            <xsl:text>&#x09;</xsl:text>
            <xsl:value-of select="$idno"/>
            <xsl:text>&#x0a;</xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="file" mode="filename_vs_title">
        <xsl:variable name="doc" select="document(@n)"/>
        <xsl:variable name="filebase" select="replace(replace(@n, 'src/', ''), '.xml', '')"/>
        <xsl:variable name="title" select="$doc//tei:title"/>
        <xsl:if test="not($title eq $filebase)">
            <xsl:text>&#x09;</xsl:text>
            <xsl:value-of select="$filebase"/>
            <xsl:text>&#x09;</xsl:text>
            <xsl:value-of select="$title"/>
            <xsl:text>&#x0a;</xsl:text>
        </xsl:if>
    </xsl:template>
    

</xsl:stylesheet>
