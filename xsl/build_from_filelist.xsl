<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:hdw="http://hdw.artsci.wustl.edu/ns"
    xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs hdw" version="2.0">


    <!-- The teibpmod templates are imported and become the default templates,
        but they are applied to each source file in turn, and not to the toc.xml file
        that lists these source files.
    -->
    <xsl:import href="teibpmod.xsl"/>

    <xsl:function name="hdw:trimsrcpath">
        <xsl:param name="path"/>
        <xsl:value-of select="replace($path, 'src/', '')"/>
    </xsl:function>

    <xsl:function name="hdw:ext_xml2html">
        <xsl:param name="path"/>
        <xsl:value-of select="replace($path, '.xml', '.html')"/>
    </xsl:function>


    <!-- The table of contents file calls just one driver template, 
         which processes each document in turn. After the root, all default 
         templates (without a mode) apply to these XML source files.
         -->
    
    <xsl:template match="/">
        <xsl:apply-templates select="files" mode="toc"/>
        <xsl:apply-templates select="files/file" mode="driver"/>
        <!--
        -->
    </xsl:template>

    <xsl:template match="file" mode="driver"> 
        <xsl:variable name="doc" select="document(@n)"/>
        <xsl:variable name="outputname">html/stelae/<xsl:value-of
                select="hdw:ext_xml2html(hdw:trimsrcpath(@n))"/></xsl:variable>
        <xsl:result-document href="{$outputname}">
            <xsl:call-template name="htmlShell">
                <xsl:with-param name="doc" select="$doc/tei:TEI"></xsl:with-param>
            </xsl:call-template>
        </xsl:result-document>
        <xsl:message
            select="concat($outputname, '&#x0a;&#x09;', $doc/tei:TEI/tei:teiHeader//tei:title, '&#x0a;&#x09;', $doc/tei:TEI/tei:teiHeader//tei:idno)"/>
    </xsl:template>

    <xsl:template match="files" mode="toc">
        <xsl:result-document href="html/index.html">
        <html>
            <xsl:call-template name="htmlHead">
                <xsl:with-param name="pagetitle">West River Inscriptions</xsl:with-param>
                <xsl:with-param name="depthpath"></xsl:with-param>
            </xsl:call-template>
            <body class="pure-g toc">
                <div id="pageheader" class="pure-u-1">
                    <xsl:call-template name="pageHeader">
                        <xsl:with-param name="pagetitle"></xsl:with-param>
                        <xsl:with-param name="depthpath"></xsl:with-param>
                    </xsl:call-template>
                </div>
                <div id="searchbox" class="pure-u-1">
                    <input type="text" id="searchterm" placeholder="Search"> </input>
                    <div id="hitcount"> </div>
                    <ul id="resultlist" class="pure-g"> </ul>
                </div>
                <div class="pure-u-1"><a href="map.html">Map</a></div>
                <div class="pure-u-1">
                    <ul class="pure-g">
                        <xsl:apply-templates select="file" mode="toc"/>
                    </ul>
                </div>
                <div id="footer" class="pure-u-1">
                    <xsl:copy-of select="$htmlFooter"/>
                </div>
                <script src="../js/jquery-3.1.1.min.js">//</script>
                <script src="../js/lunr.js">//</script>
                <script src="../js/lz-string.min.js">//</script>
                <script src="../js/search.js">//</script> 
                <script src="../js/westriver.js">//</script>
            </body>
        </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="file" mode="toc">
        <li class="pure-u-1 pure-u-sm-1-2 pure-u-m-1-4">
            <a href="stelae/{hdw:ext_xml2html(hdw:trimsrcpath(@n))}">
                <xsl:value-of select="document(@n)//tei:title[1]"/>
            </a>
        </li>
    </xsl:template>

</xsl:stylesheet>
