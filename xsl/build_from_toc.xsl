<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:hdw="http://hdw.artsci.wustl.edu/ns"
    xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="#all" version="2.0">

    <!-- The teibpmod templates are imported and become the default templates,
        but they are applied to each source file in turn, and not to the toc.xml file
        that lists these source files.
    -->
    <xsl:import href="teibpmod.xsl"/>

    <xsl:function name="hdw:trimsrcpath">
        <xsl:param name="path"/>
        <xsl:value-of select="replace($path, 'src/', '')"/>
    </xsl:function>

    <xsl:function name="hdw:ext_xml2html">
        <xsl:param name="path"/>
        <xsl:value-of select="replace($path, '.xml', '.html')"/>
    </xsl:function>


    <!-- The table of contents file calls just one driver template, 
         which processes each document in turn. After the root, all default 
         templates (without a mode) apply to these XML source files.
         -->

    <xsl:template match="/">
        <xsl:apply-templates select=".//tei:listPlace[@xml:id = 'locationlist']" mode="toc"/>
        <xsl:apply-templates select=".//tei:place[@type = 'stele']" mode="driver"/>
    </xsl:template>

    <!-- build each stelae's page -->
    <xsl:template match="tei:place[@type = 'stele']" exclude-result-prefixes="#all" mode="driver">
        <xsl:variable name="idno" select="tei:idno[1] | tei:p/tei:idno[1]"/>
        <xsl:variable name="docpath">../src/<xsl:value-of select="$idno"/>.xml</xsl:variable>
        <xsl:variable name="doc" select="document($docpath)"/>
        <xsl:variable name="outputname">html/stelae/<xsl:value-of select="$idno"
            />.html</xsl:variable>
        <xsl:variable name="mapurl">
            <xsl:choose>
                <xsl:when test=".//tei:graphic[1]/@url">
                    <xsl:value-of select=".//tei:graphic[1]/@url"/>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
        </xsl:variable>
        <xsl:result-document href="{$outputname}" method="html" indent="yes">
            <xsl:call-template name="htmlShell">
                <xsl:with-param name="doc" select="$doc/tei:TEI"/>
                <xsl:with-param name="mapurl" select="$mapurl"/>
            </xsl:call-template>
        </xsl:result-document>
        <xsl:message
            select="concat($outputname, '&#x0a;&#x09;', $doc/tei:TEI/tei:teiHeader//tei:title, '&#x0a;&#x09;', $doc/tei:TEI/tei:teiHeader//tei:idno)"
        />
    </xsl:template>

    <!-- build the list of transcriptions page -->
    <xsl:template match="tei:listPlace[@xml:id = 'locationlist']" mode="toc"
        exclude-result-prefixes="#all">
        <xsl:result-document href="html/transcriptions.html" method="html" indent="yes">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <xsl:call-template name="htmlHead">
                    <xsl:with-param name="pagetitle">West River Inscriptions</xsl:with-param>
                    <xsl:with-param name="depthpath"/>
                </xsl:call-template>
                <body class="pure-g toc">
                    <div id="pageheader" class="pure-u-1">
                        <xsl:call-template name="pageHeader">
                            <xsl:with-param name="pagetitle"/>
                            <xsl:with-param name="depthpath"/>
                        </xsl:call-template>
                        <div id="menu">
                            <a href="transcriptions.html" class="pure-button">List of transcriptions</a><xsl:text>&#xa;</xsl:text>
                            <a href="about.html" class="pure-button">About this site</a><xsl:text>&#xa;</xsl:text>
                            <a href="photos.html" class="pure-button">Photo Gallery</a><xsl:text>&#xa;</xsl:text>
                            <a href="contact.html" class="pure-button">Contact Us</a><xsl:text>&#xa;</xsl:text>
                            <input type="text" id="searchterm" placeholder="Search"> </input>
                            <div id="hitcount"> </div>
                            <ul id="resultlist" class="pure-g"> </ul>
                        </div>
                    </div>
                    <div id="searchbox" class="pure-u-1 ">
                    </div>
                    <div id="institutionlist" class="pure-u-1 pure-u-m-1-2 pure-u-lg-1-2">
                        <ul class="pure-g">
                            <xsl:apply-templates select=".//tei:place[@type = 'institution']"
                                mode="toc"/>
                        </ul>
                    </div>
                    <div id="mastermap" class="pure-u-1 pure-u-m-1-2 pure-u-lg-1-2" style="margin-top: 50px; height: 700">
                    </div>
                    <div id="footer" class="pure-u-1">
                        <xsl:copy-of select="$htmlFooter"/>
                    </div>
                    <script src="js/jquery-3.1.1.min.js">//</script>
                    <script src="js/myconv.js" type="text/javascript" charset="utf-8">//</script>
                    <script src="js/search.js">//</script>
                    <script src="js/buildmap.js">//</script>
                    <script src="js/westriver.js">//</script>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="//tei:place[@type = 'stele']" mode="toc">
        <li class="pure-u-1">
            <a href="stelae/{.//tei:idno[1]}.html">
                <xsl:value-of select=".//tei:placeName[1]"/>
            </a>
        </li>
    </xsl:template>

    <xsl:template match="//tei:place[@type = 'institution']" mode="toc">
        <li class="pure-u-1 pure-u-sm-1 pure-u-m-1 pure-u-lg-1 l-box">
            <xsl:value-of select="tei:placeName | tei:ab/tei:placeName"/>
            <ul class="pure-g">
                <xsl:apply-templates select=".//tei:place[@type = 'stele']" mode="toc"/>
            </ul>
        </li>
    </xsl:template>






</xsl:stylesheet>
