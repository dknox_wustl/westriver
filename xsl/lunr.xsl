<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:hdw="http://hdw.artsci.wustl.edu/ns"
  xmlns:tei="http://www.tei-c.org/ns/1.0" version="2.0" exclude-result-prefixes="tei">
  <xsl:output method="text" omit-xml-declaration="yes"/>

<!--
  <xsl:param name="stopwords" select="concat('((\s|^)(', replace('also and are bizet bars been but copy copies date des est et for from has had have he her him his is la le les not one page pages printed que score she the they this that two was were which who work with', ' ', '|'), ')+)(\s|$)')"/>
  -->
  <xsl:param name="stopwords">also and</xsl:param>
  
  <!-- top-level driver -->
  <xsl:template match="/">
    [
    <xsl:apply-templates select="files/file"/>
    ]
  </xsl:template>

  <xsl:template match="files/file">
    <xsl:variable name="filepath" select="@n"/>
    {"id": "<xsl:value-of select="$filepath"/>",
    "title": "<xsl:value-of
      select="document($filepath)//tei:titleStmt/tei:title[1]"/>",
   <xsl:variable name="textcontent" 
     select="document($filepath)//tei:body"/>"content": "<xsl:value-of select="
       hdw:stringprocess($textcontent)"/>"}<xsl:if test="not(position()=last())">,</xsl:if>
  </xsl:template>


  <xsl:function name="hdw:removenonwordchars">
    <xsl:param name="input"/>
    <xsl:value-of select="replace($input, '[^\w ]+', ' ')"/>
  </xsl:function>
  
  <xsl:function name="hdw:collapsewhitespace">
    <xsl:param name="input"/>
    <xsl:value-of select="replace($input, '\s+', ' ')"/>
  </xsl:function>
  
  <xsl:function name="hdw:removeshortwords">
    <xsl:param name="input"/>
    <!--
    <xsl:value-of select="replace($input, '(\s.{0,2})+\s', ' ')"/>
    -->
    <xsl:value-of select="$input"/>
  </xsl:function>
  
  <xsl:function name="hdw:removestopwords">
    <xsl:param name="input"/>
    <xsl:value-of select="replace($input, $stopwords, ' ')"/>
  </xsl:function>
  
  <xsl:function name="hdw:stringprocess">
    <xsl:param name="input"/>
    <xsl:value-of select="string-join(
        distinct-values(
        tokenize(
        normalize-space(
        hdw:removestopwords(
        hdw:removeshortwords(
        hdw:collapsewhitespace(
        lower-case(
        hdw:removenonwordchars($input)))))), '\s+' )), ' ')"/>
  </xsl:function>
  

</xsl:stylesheet>
