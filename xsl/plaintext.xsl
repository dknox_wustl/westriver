<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:html="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xsl tei #default">

	<xsl:output method="text" omit-xml-declaration="yes"/>
	<xsl:template match="tei:teiHeader"/>	


</xsl:stylesheet>
