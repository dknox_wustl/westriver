<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:html="http://www.w3.org/1999/xhtml" 
    exclude-result-prefixes="xs" version="2.0">

    <xsl:template match="/">
        <html>
            <head>
                <title>West River Inscriptions:
                    <xsl:value-of select="//tei:titleStmt//tei:title[1]   "/>
                </title>
                <link rel="stylesheet" href="/css/picnic.min.css"/>
                <link rel="stylesheet" href="/css/westriver.css"/>
            </head>
            <body>
                <h1>West River Inscriptions:
                    <xsl:value-of select="//tei:titleStmt//tei:title[1]   "/>
                </h1>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>



</xsl:stylesheet>
