<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output omit-xml-declaration="yes"/>
    
    <!-- check toc.xml (the source) against filelist.xml to be sure that:
            each place with an idno in toc.xml must match a filelist xml file name
            and vice versa -->
    
    <xsl:variable name="filelist" select='document("../filelist.xml")/files'/>
    <xsl:variable name="tocfile" select='/'/>
    
    <xsl:template match="/">
        <!-- use toc.xml to check filelist.xml -->
        <xsl:message>Checking filelist against toc</xsl:message>
        <xsl:apply-templates select="//tei:place//tei:idno"/>  
        <xsl:message>Checking toc against filelist</xsl:message>
        <xsl:apply-templates select="$filelist/file" mode="filelist_driver"/>  <!-- use filelist.xml to check toc.xml -->
    </xsl:template>
    
    
    <xsl:template match="//tei:place//tei:idno">
        <xsl:variable name="expectedpath">src/<xsl:value-of select="."/>.xml</xsl:variable>
        <xsl:variable name="matchedpath" select="$filelist/file[@n=$expectedpath]/@n"/>
        <xsl:if test="$expectedpath ne $matchedpath">   No match for <xsl:value-of select="$expectedpath"/>&#x0a;</xsl:if>
    </xsl:template>
    
    <xsl:template match="file" mode="filelist_driver">
        <xsl:variable name="expectedidno" select="replace(replace(@n, 'src/', ''), '.xml', '')"/>
        <xsl:variable name="matchedidno" select="$tocfile//tei:idno[.=$expectedidno]"/>
        <xsl:if test="$expectedidno ne $matchedidno">   No match for <xsl:value-of select="$expectedidno"/>&#x0a;</xsl:if>
    </xsl:template>
    

</xsl:stylesheet>
