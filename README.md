# Information on building HTML for the West River Project
As of 1/6/2017

## General plan

The master files (XML and images) are currently on Box.com.

This build process presumes working on a build machine that need not be (and on principle probably ought not to be) the deployment machine. The steps go like this:

1) Copy the Box files to a local development directory. Here I'm going to make temporary symbolic links to the Box directories we need:

    mkdir -p ~/prep; cd ~/prep
    ln -s ~/box/West\ River\ stone\ inscriptions/[B-X]*/ .

2) Checkout the build repository in the same location (which probably need an SSH key set-up to work):

(**SD note:** cd to directory where you saved box files and then run this command. Then copy files into directory copied from box. OR, just use Downloads function from BitBucket)

    git clone git@bitbucket.org:dknox_wustl/westriver.git

3) Then start the build process using the scripts outlined below. Without validation etc., a minimal sequence might look like this:

(**SD note:** must install imagemagick and jq for scripts to work. Also, modified teibpmod.xsl to include DLS' Google Analytics tracking code.)

    cd westriver
    bin/cp_images_src.sh
    bin/makethumbs.sh
    bin/makefilelist.sh
    bin/makedocs_from_toc.sh
    bin/make_text_index.sh

4) The contents of the `html` directory can then be pushed as the root directory of the web project on a deployment server.

## Software dependencies

`bash` shell (or `zsh`)

`saxonhe` throughout for XSLT 2.0 transformations

`jing` for XML validation

`jq` for minifying the index

`convert` from ImageMagick for thumbnail creation

## File structure

Original images and XML files are organized in directories according to site. These files have been managed on Box.com in 2016, and are copied to a local file system for processing. Each site directory begins with a capital letter. For more info on Box, see below.

Processing code is in a directory called `westriver` at the same level:

| `westriver/`
| `BaidanWenwudian/`
| `BaiseYuedonghuiguan/`
| … [etc.] …
| `XishanLonghuasi/`

The `westriver` directory repository includes some directories and files with scripts and other fixed data, an empty `src` directory, a backup `srcbackup` directory, and a build directory (html) with fixed CSS and JavaScript as well as derived HTML produced in the build process. In slightly more detail, the directories are as follows:

`/bin` --- scripts for checking files, processing, and creating derivatives

`/xsl` --- XSL stylesheets

`/src` --- a empty directory into which source XML files are copied, flattened to one level if there was any hierarchy in the original directories.

`/srcbackup` --- a copy of the XML files; the intent here is that these can be committed to the Git repository for safe keeping, though the images will not be.

`/html` --- the root of the resulting public files for the web server, organized as follows:

`/html/css` --- fixed CSS files

`/html/js` --- fixed JavaScript files

`/html/images` --- a flat collection of image files copied from the original directories

`/html/images/thumbnails` --- a flat collection of derived thumbnails

`/html/stelae` --- a flat collection of derived HTML files, each a thin HTML wrapper around a lightly transformed XML. This is similar to a static version of what TEI Boilerplate does, but it does not rely on the browser for XSLT transformation.

There are a few fixed files that matter, and one derived file:

`/toc.xml` --- this is a new XML master table of contents file hand-crafted from Steve Miles's Word document. It is *not* derived automatically from anything; it's where the map links currently live, and it drives the creation of XML files.

`/filelist.xml` --- a derived schemaless XML file that consists solely of pointers to XML files under `src`

`/README.md` --- the source of this README.

## Scripts

These scripts presume that an executable saxonhe is available on the path, and also assume that `westriver` is a sibling to site directories that contain images and XML files (perhaps in a hierarchical form). They also presume that all such directories (and only those) start with a capital letter. Many of these scripts set shell variables *\$bindir*, *\$hubdir*, and *\$localdir*, corresponding to the `bin` script directory and its parent and grandparent folders, so that the scripts can be called from anywhere and find their way.

`checkdiffs.sh` --- Assuming that the Box files are mounted in a particular location away from the current directory structure, compare those files to our local "original" copies, and report any differences.

`ln_images_src.sh` --- Create symbolic links for JPEG images in /images and symbolic links for XML files in `src`. This has an alternative option:

`cp_images_src.sh` --- Create hard copies of images in /images and symbolic links for XML files in `src`. This is an alternative to the above.

`makethumbs.sh` --- For each image in html/images, it creates a 300px-wide thumbnail in html/images/thumbnails.

`makefilelist.sh` --- From a flat list of XML files in `src`, create a schemaless XML listing in westriver/filelist.xml

`validate.sh` --- For each XML file in `src`, validate against a TEI schema and report any errors.

`testreport.sh` --- Compare filenames, idnos in XML, and titles in XML, so that one might see obvious discrepancies. 

`checkfilelist.sh` --- Compare toc.xml against filelist.xml to be sure we have what we want and aren't missing anything.

`makedocs_from_filelist.sh` [ DEPRECATED ] --- Create an HTML index file from filelist.xml; create html subdirectories and HTML files for each XML source file. This is being superseded by the following:

`makedocs_from_toc.sh` --- Create an HTML index file from toc.xml; create html subdirectories and HTML files for each XML source file. 

`make_text_index.sh` --- Create a compact full-text index in html/js/westriverindex.json. 


## Box

At the moment (January 2017) the Box files are the source of truth for XML and for images. On Windows or Mac they presumably show up as part of the filesystem when the Box client is installed. On Linux it's possible to mount Box through WebDav. I found that the instructions here worked for me: [https://kb.iu.edu/d/bcim](https://kb.iu.edu/d/bcim). You have to "change your password" in Box account settings, which seems really to mean just setting your "external password" for purposes like this. It doesn't change the WUSTL Key login routine for the web interface. One can add a line like this to `/etc/fstab` at a mount point of one's choosing:

    https://dav.box.com/dav/ /home/knoxdw/box  davfs  rw,user,noauto 0 0%

… and then one can simply use `mount ~/box` and `umount ~/box` as desired.

## Conclusion

When done, the `html` directory ought to have everything necessary for a static site.

To generate this document in PDF form:

    pandoc README.md -f markdown -t latex -o readme.pdf