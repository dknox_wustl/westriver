echo "Checking Box copy vs. local copy:"
cd $(dirname "$0")/..

for d in  /mnt/box/West\ River\ stone\ inscriptions/[B-X]*/;
do
    base=$(basename "$d");
    echo "$base";
    diff -r "$d" ../"$base";
done

