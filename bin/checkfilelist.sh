#/bin/sh
#
# check toc.xml (the source) against filelist.xml to be sure that:
# each place with an idno in toc.xml must match a filelist xml name
# and vice versa

cd $(dirname "$0")/..
saxonhe="java -jar bin/saxon9he.jar"
${saxonhe} -xsl:xsl/filelistcheck.xsl -s:toc.xml

