cd $(dirname "$0")/..
saxonhe="java -jar bin/saxon9he.jar"
${saxonhe} -xsl:xsl/lunr.xsl -s:filelist.xml  -o:html/js/westriverindex_pp.json

# make a compact version from the prettyprint version
# -- depends of course on installing jq
jq -c "." html/js/westriverindex_pp.json >html/js/westriverindex.json

