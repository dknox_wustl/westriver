cd $(dirname "$0")/..

mkdir -p html/images
mkdir -p src     # for XML
rm src/*         # make sure target directories are empty;
rm html/images/thumbnails/*.*
rm html/images/*.*

find -L ../[A-Z]*/ -iname "*.jpg"  -exec ln -rs {} html/images/ \;
rename "s/JPG/jpg/" html/images/*.jpg

find -L ../[A-Z]*/ -iname "*.xml"  -exec ln -rs {} src/ \;

# src is the fresh working copy of the XML;
# srcbackup is a copy (to commit to Git repo for safety)

mkdir -p srcbackup
cp src/* srcbackup/
