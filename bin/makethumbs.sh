cd $(dirname "$0")/..
mkdir -p html/images/thumbnails
for i in html/images/*.jpg;
do
    b=$(basename $i);
    echo $b
    convert $i -resize 300X html/images/thumbnails/$b;
done

